define([
    'jquery',
    'underscore',
    'Magento_Catalog/js/price-utils',
    'mage/translate',
    'Magento_Ui/js/block-loader',
    'priceOptions',
    'priceOptionFile',
    'jquery/ui',
    'jquery/jquery.parsequery',
    'Magento_Swatches/js/swatch-renderer'
], function ($, _, priceUtils, $t, blockLoader) {
    'use strict';

    $.widget('sz.Sdcp', $.mage.SwatchRenderer, {
        options: {
            sdcp_identifier: {
                sku: '[itemprop=sku]',
                name: '[itemprop=name]',
                fullDesc: {
                    label: '#tab-label-product\\.info\\.description',
                    content: '.product.attribute.description .value',
                    blockContent: '#product\\.info\\.description'
                },
                shortDesc: '.product.attribute.overview',
                stock: '.stock.available span',
                addtocart_button: '#product-addtocart-button',
                increment: '.product.pricing',
                qty_box: '#qty',
                tier_price: '.prices-tier.items',
                additionalInfo: {
                    label: '#tab-label-additional',
                    content: '#additional'
                },
            },
        },
        _RenderControls: function () {
            if (this.options.jsonModuleConfig['meta_data'] > 0) {
                this.options.jsonChildProduct['meta_data']['meta_title'] =
                (this.options.jsonChildProduct['meta_data']['meta_title'] == null) ?
                document.title :
                this.options.jsonChildProduct['meta_data']['meta_title'];

                this.options.jsonChildProduct['meta_data']['meta_description'] =
                (this.options.jsonChildProduct['meta_data']['meta_description'] == null) ?
                $('head meta[name="description"]').attr('content') :
                this.options.jsonChildProduct['meta_data']['meta_description'];

                if ($('head meta[name="keywords"]').length > 0 && this.options.jsonChildProduct['meta_data']['meta_keyword'] == null) {
                    this.options.jsonChildProduct['meta_data']['meta_keyword'] = $('head meta[name="keywords"]').attr('content');
                }
            }

            this._super();

            this._UpdateSelected(this.options, this);
            // this._UpdatePrice();
        },
        _EventListener: function () {
            this._super();
            this._ValidateQty(this);
        },
        _OnClick: function ($this, $widget) {
            
            var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                $label = $parent.find('.' + $widget.options.classes.attributeSelectedOptionLabelClass),
                attributeId = $parent.attr('attribute-id'),
                $input = $widget.element.closest('form').find(
                    '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                );

            if ($widget.inProductList) {
                $input = $widget.productForm.find(
                    '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                );
            }

            if ($this.hasClass('disabled')) {
                return;
            }

            if ($this.hasClass('selected')) {
                $parent.removeAttr('option-selected').find('.selected').removeClass('selected');
                $input.val('');
                $label.text('');
                //sz_Simpledetailconfigurable
                $widget._ResetDetail();
            } else {
                $parent.attr('option-selected', $this.attr('option-id')).find('.selected').removeClass('selected');
                $label.text($this.attr('option-label'));
                $input.val($this.attr('option-id'));
                $this.addClass('selected');
            }

            $widget._Rebuild();
            //sz
            if ($widget.element.parents($widget.options.selectorProduct)
                    .find(this.options.selectorProductPrice).is(':data(mage-priceBox)')
            ) {
                $widget._UpdatePrice();
            }
            $widget._UpdateDetail();
            $input.trigger('change');
        },
        _OnChange: function ($this, $widget) {
            var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                attributeId = $parent.attr('attribute-id'),
                $input = $widget.element.closest('form').find(
                    '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                );

            if ($widget.inProductList) {
                $input = $widget.productForm.find(
                    '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                );
            }

            if ($this.val() > 0) {
                $parent.attr('option-selected', $this.val());
                $input.val($this.val());
            } else {
                $parent.removeAttr('option-selected');
                $input.val('');
            }

            $widget._Rebuild();
            //sz
            $widget._UpdatePrice();
            $widget._UpdateDetail();
            $input.trigger('change');
        },
        _UpdateDetail: function () {
            var $widget = this,
                index = '',
                childProductData = this.options.jsonChildProduct,
                moduleConfig = this.options.jsonModuleConfig,
                keymap,
                url = '',
                super_attribute = {};
            if (childProductData['is_ajax_load'] > 0) {
                $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                    super_attribute[$(this).attr('attribute-id')] = $(this).attr('option-selected');
                    index += $(this).attr('option-selected') + '_';
                    url += '+' + $(this).attr('attribute-code') + '-'
                    + $widget.options.jsonChildProduct['map'][$(this).attr('attribute-id')][$(this).attr('option-selected')];
                });
                if (childProductData['child'][index] === undefined) {
                    $.ajax({
                        url: $widget.options.ajaxUrl,
                        type: 'POST',
                        data: $.param({
                            product_id: childProductData['entity'],
                            super_attribute: super_attribute
                        }),
                        dataType: 'json',
                        showLoader: true,
                        success : function (data) {
                            $widget.options.jsonChildProduct['child'][index] = data;
                            if (data !== false) {
                                $widget._UpdateUrl(
                                    childProductData['url'],
                                    url,
                                    moduleConfig['url'],
                                    moduleConfig['url_suffix']
                                );
                                $widget._UpdatePriceAjax(data['price']);
                                $widget._UpdateUrl(
                                    childProductData['url'],
                                    url,
                                    moduleConfig['url'],
                                    moduleConfig['url_suffix']
                                );
                                $widget._UpdateDetailData(data);
                            } else {
                                $widget._UpdatePriceAjax(childProductData['price']);
                                $widget._ResetDetail();
                            }
                        }
                    });
                } else if (childProductData['child'][index] !== false) {
                    $widget._UpdatePriceAjax(childProductData['child'][index]['price']);
                    $widget._UpdateUrl(
                        childProductData['url'],
                        url,
                        moduleConfig['url'],
                        moduleConfig['url_suffix']
                    );
                    $widget._UpdateDetailData(childProductData['child'][index]);
                } else {
                    $widget._UpdatePriceAjax(childProductData['price']);
                    $widget._ResetDetail();
                    return false;
                }
            } else {
                $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                    index += $(this).attr('option-selected') + '_';
                    url += '+' + $(this).attr('attribute-code') + '-'
                    + $widget.options.jsonChildProduct['map'][$(this).attr('attribute-id')][$(this).attr('option-selected')];
                });
                if (!childProductData['child'].hasOwnProperty(index)) {
                    $widget._ResetDetail();
                    return false;
                }
                $widget._UpdateUrl(
                    childProductData['url'],
                    url,
                    moduleConfig['url'],
                    moduleConfig['url_suffix']
                );
                $widget._UpdateDetailData(childProductData['child'][index]);
            }
        },
        _UpdateDetailData: function (data) {
            var moduleConfig = this.options.jsonModuleConfig,
                childProductData = this.options.jsonChildProduct,
                $widget = this;

            $widget._UpdateSku(data['sku'], moduleConfig['sku']);
            
            $widget._UpdateName(data['name'], moduleConfig['name']);

            $widget._UpdateDesc(
                data['desc'],
                data['sdesc'],
                moduleConfig['desc']
            );

            $widget._UpdateAdditionalInfo(
                data['additional_info'],
                moduleConfig['additional_info']
            );

            $widget._UpdateMetaData(
                data['meta_data'],
                childProductData['meta_data'],
                moduleConfig['meta_data']
            );

            $widget._UpdateStock(
                data['stock_status'],
                data['stock_number'],
                moduleConfig['stock']
            );

            $widget._UpdateTierPrice(
                data['price']['tier_price'],
                data['price']['basePrice'],
                moduleConfig['tier_price']
            );

            $widget._UpdateIncrement(
                data['increment'],
                data['name'],
                moduleConfig['increment']
            );

            $widget._UpdateMinQty(
                data['minqty'],
                moduleConfig['min_max']
            );
            
            $widget._UpdateImage(
                data['image'],
                moduleConfig['images']
            );
        },
        _UpdateSku: function ($sku, $config) {
            if ($config > 0) {
                $('[itemprop=sku]').html($sku);
            }
        },
        _UpdateName: function ($name, $config) {
            if ($config > 0) {
                $('[itemprop=name]').html($name);
            }
        },
        _UpdateDesc: function ($desc, $sdesc, $config) {
            if ($config > 0) {
                this._UpdateFullDesc($desc);
                this._UpdateShortDesc($sdesc);
            }
        },
        _UpdateFullDesc: function ($desc) {
            var html;
            if ($desc) {
                if ($(this.options.sdcp_identifier.fullDesc.label).css('display') != 'none') {
                    $(this.options.sdcp_identifier.fullDesc.content).html($desc);
                } else {
                    $('.data.item.title').removeClass("active");
                    $('.data.item.content').css('display', 'none');
                    $(this.options.sdcp_identifier.fullDesc.label).css('display', 'inline-block').addClass("active");
                    $(this.options.sdcp_identifier.fullDesc.blockContent).css('display', 'block');
                    $(this.options.sdcp_identifier.fullDesc.content).html($desc);
                }
            } else {
                $(this.options.sdcp_identifier.fullDesc.label).css('display', 'none').removeClass("active");
                $(this.options.sdcp_identifier.fullDesc.blockContent).css('display', 'none');
            }
        },
        _UpdateShortDesc: function ($sdesc) {
            var html;
            if ($sdesc) {
                if ($(this.options.sdcp_identifier.shortDesc).find('.value').length) {
                    $(this.options.sdcp_identifier.shortDesc).find('.value').html($sdesc);
                    $(this.options.sdcp_identifier.shortDesc).fadeIn();
                } else {
                    html = '<div class="product attribute overview">'
                    + '<div class="value" itemprop="description">'
                    + $sdesc
                    + '</div></div>';
                    $(this.options.selectorProduct).append(html);
                }
            } else {
                $(this.options.sdcp_identifier.shortDesc).fadeOut();
            }
        },
        _UpdateStock: function ($status, $number, $config) {
            if ($config > 0) {
                var stock_status = '';
                if ($status > 0) {
                    stock_status = $t('IN STOCK');
                    $(this.options.sdcp_identifier.addtocart_button).removeAttr('disabled');
                } else {
                    stock_status = $t('OUT OF STOCK');
                    $(this.options.sdcp_identifier.addtocart_button).attr('disabled', 'disabled');
                }
                stock_status += " - " + Number($number);
                $(this.options.sdcp_identifier.stock).html(stock_status);
            }
        },
        _UpdateIncrement: function ($increment, $name, $config) {
            $(this.options.sdcp_identifier.increment).remove();
            if ($config > 0 && $increment > 0) {
                var html = '<div class="product pricing">';
                html += $t('%1 is available to buy in increments of %2').replace('%1', $name).replace('%2', $increment);
                html += '</div>';
                $(this.options.selectorProduct).append(html);
            }
        },
        _UpdateMinQty: function ($value, $config) {
            if ($config > 0) {
                if ($value > 0) {
                    $(this.options.sdcp_identifier.qty_box).val($value);
                    $(this.options.sdcp_identifier.qty_box).trigger('change');
                } else {
                    $(this.options.sdcp_identifier.qty_box).val(1);
                    $(this.options.sdcp_identifier.qty_box).trigger('change');
                }
            }
        },
        _UpdateTierPrice: function ($priceData, $basePrice, $moduleConfig) {
            if ($moduleConfig > 0) {
                var $widget = this,
                    percent,
                    html = '',
                    htmlTierPrice = '',
                    have_tier_price = false,
                    htmlTierPrice4 = '<span class="percent tier-%4">&nbsp;%5</span>%</strong>',
                    htmlTierPrice5 = '<span class="price-container price-tier_price tax weee"><span data-price-amount="%2" data-price-type="" class="price-wrapper "><span class="price">%3</span></span></span>';
                $(this.options.sdcp_identifier.tier_price).remove();
                html = '<ul class="prices-tier items">';
                $.each($priceData, function (key, vl) {
                    percent = Math.round((1 - Number(vl['base'])/Number($basePrice)) * 100);
                    if (percent == 0) {
                        percent = ((1 - Number(vl['base'])/Number($basePrice)) * 100).toFixed(2);
                    }
                    have_tier_price = true;
                    htmlTierPrice = $t('Buy %1 for ').replace('%1', Number(vl['qty']));
                    htmlTierPrice += htmlTierPrice5.replace('%2', Number(vl['value'])).replace('%3', $widget._getFormattedPrice(Number(vl['value'])));
                    htmlTierPrice += $t(' each and ');
                    htmlTierPrice += '<strong class="benefit">';
                    htmlTierPrice += $t('save');
                    htmlTierPrice += htmlTierPrice4.replace('%4', key).replace('%5', percent);
                    html += '<li class="item">';
                    html += htmlTierPrice;
                    html += '</li>';
                });
                html += '</ul>';
                if (have_tier_price) {
                    $('.product-info-price').after(html);
                }
            }
        },
        _UpdateImage: function (images, $config) {
            var justAnImage = images[0],
                updateImg,
                $this = this.element,
                imagesToUpdate,
                context = $this.parents('.column.main'),
                gallery = context.find(this.options.mediaGallerySelector).data('gallery'),
                item;
            if ($config < 1) {
                if (this.options.onlyMainImg) {
                    var widget = this;
                    $.each(images, function ($id, $vl) {
                        if ($vl.isMain) {
                            imagesToUpdate = widget.options.jsonChildProduct['image'];
                            imagesToUpdate[0] = $vl;
                            return true;
                        }
                    });
                    images = imagesToUpdate;
                } else {
                    images = images.concat(this.options.jsonChildProduct['image']);
                }
            }
            imagesToUpdate = images.length ? this._setImageType($.extend(true, [], images)) : [];
            gallery.updateData(images);
            gallery.first();
        },
        _UpdateAdditionalInfo: function ($info, $config) {
            var html = '';
            if ($config > 0) {
                if (Object.keys($info) != '') {
                    $.each($info, function ($id, $vl) {
                        html += '<tr>'
                            + '<th class="col label" scope="row">' + $vl['label'] + '</th>'
                            + '<td class="col data" data-th="' + $vl['label'] + '">' + $vl['value'] + '</td>'
                            + '</tr>';
                    });
                    if ($(this.options.sdcp_identifier.additionalInfo.label).css('display') != 'none') {
                        $('.data.item.title').removeClass("active");
                        $('.data.item.content').css('display', 'none');
                        $(this.options.sdcp_identifier.additionalInfo.label).addClass('active').css('display', 'inline-block');
                        $(this.options.sdcp_identifier.additionalInfo.content).css('display', 'block').find('tbody').html(html);
                    } else {
                        $('.data.item.title').removeClass("active");
                        $('.data.item.content').css('display', 'none');
                        $(this.options.sdcp_identifier.additionalInfo.label).css('display', 'inline-block').addClass('active');
                        $(this.options.sdcp_identifier.additionalInfo.content).css('display', 'block').find('tbody').html(html);
                    }
                } else {
                    $('.data.item.title').removeClass("active");
                    $('.data.item.content').css('display', 'none');
                    $(this.options.sdcp_identifier.additionalInfo.label).css('display', 'none');
                }
            }
            if ($(window.location).attr('hash') == '') {
                $('.data.item.title').first().trigger('click');
            }
        },
        _UpdateMetaData: function ($metaData, $parentMetaData, $config) {
            if ($config > 0) {
                if ($metaData['meta_description'] != null) {
                    $('head meta[name="description"]').attr('content', $metaData['meta_description']);
                } else {
                    $('head meta[name="description"]').attr('content', $parentMetaData['meta_description']);
                }
                if ($metaData['meta_keyword'] != null) {
                    if ($('head meta[name="keywords"]').length > 0) {
                        $('head meta[name="keywords"]').attr('content', $metaData['meta_keyword']);
                    } else {
                        $('head meta[name="description"]').after(
                            '<meta name="keywords" content="' + $metaData['meta_keyword'] + '" />'
                        );
                    }
                } else {
                    if ($parentMetaData['meta_keyword'] != null) {
                        if ($('head meta[name="keywords"]').length > 0) {
                            $('head meta[name="keywords"]').attr('content', $parentMetaData['meta_keyword']);
                        } else {
                            $('head meta[name="description"]').after(
                                '<meta name="keywords" content="' + $parentMetaData['meta_keyword'] + '" />'
                            );
                        }
                    } else {
                        $('head meta[name="keywords"]').remove();
                    }
                }
                if ($metaData['meta_title'] != null) {
                    document.title = $metaData['meta_title'];
                } else {
                    document.title = $parentMetaData['meta_title'];
                }
            }
        },
        _UpdateUrl: function ($parentUrl, $customUrl, $config, $suffix) {
            if ($config > 0) {
                while ($customUrl.indexOf(' ') >= 0) {
                    $customUrl = $customUrl.replace(" ", "~");
                }
                $parentUrl = $parentUrl.substring(0, $parentUrl.indexOf($suffix));
                var url = $parentUrl + $customUrl
                window.history.replaceState('SDCP', 'SCDP', url);
            }
        },
        _UpdateSelected: function ($options, $widget) {
            var config = $options.jsonModuleConfig,
            data = $options.jsonChildProduct,
            customUrl = window.location.pathname,
            selectingAttr = [],
            attr,
            selectedAttr = customUrl.split('+'),
            rootUrl = customUrl.split('+'),
            flag = false,
            $code,
            $value;

            rootUrl = rootUrl.slice(0,1);
            selectedAttr.shift();
            if (config['url'] > 0 && selectedAttr.length > 0) {
                flag = true;
                // this.options.jsonChildProduct.url = rootUrl[0];
                $.each(selectedAttr, function ($index, $vl) {
                    if (typeof $vl === 'string') {
                        $code = $vl.substring(0, $vl.indexOf('-'));
                        $value = $vl.substring($code.length + 1);
                        while ($value.indexOf('~') >= 0) {
                            $value = $value.replace("~", " ");
                        }
                        try {
                            if ($('.swatch-attribute[attribute-code="'
                                + $code
                                + '"] .swatch-attribute-options').children().is('div')) {
                                $('.swatch-attribute[attribute-code="'
                                + $code
                                + '"] .swatch-attribute-options [option-label="'
                                + decodeURIComponent($value)
                                + '"]').trigger('click');
                            } else {
                                $.each($('.swatch-attribute[attribute-code="'
                                + $code
                                + '"] .swatch-attribute-options select option'), function ($index2, $vl2) {
                                    if ($vl2.text == decodeURIComponent($value)) {
                                        $('.swatch-attribute[attribute-code="'
                                        + $code
                                        + '"] .swatch-attribute-options select').val($vl2.value).trigger('change');
                                        return true;
                                    }
                                });
                            }
                        } catch (e) {
                            console.log($.mage.__('Error when get product from urls'));
                        }
                    }
                });
            } else {
                this.options.jsonChildProduct.url = customUrl;
                if (config['preselect'] > 0 && data['preselect']['enabled'] > 0) {
                    flag = true;
                    $.each(data['preselect']['data'], function ($index, $vl) {
                        try {
                            if ($('.swatch-attribute[attribute-id='
                                + $index
                                + '] .swatch-attribute-options').children().is('div')) {
                                $('.swatch-attribute[attribute-id='
                                + $index
                                + '] .swatch-attribute-options [option-id='
                                + $vl
                                + ']').trigger('click');
                            } else {
                                $('.swatch-attribute[attribute-id='
                                + $index
                                + '] .swatch-attribute-options select').val($vl).trigger('change');
                            }
                        } catch (e) {
                            console.log($.mage.__('Error when applied preselect product'));
                        }
                    });
                }
            }
            if (flag) {
                try {
                    var justAnImage,
                        images,
                        imagesToUpdate,
                        context = $widget.element.parents('.column.main'),
                        gallery = context.find($widget.options.mediaGallerySelector),
                        item, keymap, index = '';
                    $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                        index += $(this).attr('option-selected') + '_';
                    });
                    images = data['child'][index]['image'];
                    if (config['images'] > 0) {
                        justAnImage = images[0];
                        gallery.on('gallery:loaded', function () {
                            imagesToUpdate = images.length ? $widget._setImageType($.extend(true, [], images)) : [];
                            gallery.data('gallery').updateData(images);
                        });
                    } else {
                        if ($widget.options.onlyMainImg) {
                            $.each(images, function ($id, $vl) {
                                if ($vl.isMain) {
                                    imagesToUpdate = $widget.options.jsonChildProduct['image'];
                                    imagesToUpdate[0] = $vl;
                                    return true;
                                }
                            });
                            images = imagesToUpdate;
                        } else {
                            images = images.concat($widget.options.jsonChildProduct['image']);
                        }
                        gallery.on('gallery:loaded', function () {
                            imagesToUpdate = images.length ? $widget._setImageType($.extend(true, [], images)) : [];
                            gallery.data('gallery').updateData(images);
                            gallery.data('gallery').first();
                        });
                    }
                } catch ($e) {
                    console.log($.mage.__('Error when load images of preselect product'));
                }
            }
        },
        _ValidateQty: function ($widget) {
            var keymap, index,
            data = $widget.options.jsonChildProduct,
            config = $widget.options.jsonModuleConfig,
            state;
            $('input.input-text.qty').change(function () {
                index = '';
                $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                    index += $(this).attr('option-selected') + '_';
                });
                if (data['child'].hasOwnProperty(index) && data['child'][index]['stock_status'] > 0) {
                    state = data['child'][index]['stock_status'] > 0;
                    if (config['min_max'] > 0) {
                        state = state && (data['child'][index]['minqty'] == 0 || $(this).val() >= data['child'][index]['minqty'])
                        && (data['child'][index]['maxqty'] == 0 || $(this).val() <= data['child'][index]['maxqty']);
                    }
                    if (config['increment'] > 0) {
                        state = state && (data['child'][index]['increment'] == 0 || $(this).val() % data['child'][index]['increment'] == 0);
                    }
                    if (!state) {
                        $($widget.options.sdcp_identifier.addtocart_button).attr('disabled', 'disabled');
                    } else {
                        $($widget.options.sdcp_identifier.addtocart_button).removeAttr('disabled');
                    }
                }
            });
        },
        _ResetDetail: function () {
            var moduleConfig = this.options.jsonModuleConfig;
            this._ResetSku(moduleConfig['sku']);
            this._ResetName(moduleConfig['name']);
            this._ResetDesc(moduleConfig['desc']);
            this._ResetStock(moduleConfig['stock']);
            this._ResetTierPrice(moduleConfig['tier_price']);
            this._ResetUrl(moduleConfig['url']);
            this._ResetIncrement(moduleConfig['increment']);
            this._UpdateAdditionalInfo(
                this.options.jsonChildProduct['additional_info'],
                moduleConfig['additional_info']
            );
            this._ResetMetaData(moduleConfig['meta_data']);
            this._ResetImage(moduleConfig['images']);
        },
        _ResetSku: function ($config) {
            if ($config > 0) {
                $(this.options.sdcp_identifier.sku).html(this.options.jsonChildProduct['sku']);
            }
        },
        _ResetName: function ($config) {
            if ($config > 0) {
                $(this.options.sdcp_identifier.name).html(this.options.jsonChildProduct['name']);
            }
        },
        _ResetDesc: function ($config) {
            if ($config > 0) {
                $(this.options.sdcp_identifier.fullDesc.content).html(this.options.jsonChildProduct['desc']);
                $(this.options.sdcp_identifier.shortDesc).find('.value').html(this.options.jsonChildProduct['sdesc']);
            }
        },
        _ResetStock: function ($config) {
            if ($config > 0) {
                var stock_status = '';
                if (this.options.jsonChildProduct['stock_status'] > 0) {
                    stock_status = $t('IN STOCK');
                    $(this.options.sdcp_identifier.addtocart_button).removeAttr('disabled');
                } else {
                    stock_status = $t('OUT OF STOCK');
                    $(this.options.sdcp_identifier.addtocart_button).attr('disabled', 'disabled');
                }
                $(this.options.sdcp_identifier.stock).html(stock_status);
            }
        },
        _ResetTierPrice: function ($config) {
            if ($config > 0) {
                $(this.options.sdcp_identifier.tier_price).remove();
            }
        },
        _ResetIncrement: function ($config) {
            if ($config > 0) {
                $(this.options.sdcp_identifier.increment).remove();
            }
        },
        _ResetMetaData: function ($config) {
            var $metaData = this.options.jsonChildProduct['meta_data']
            if ($config > 0) {
                $('head meta[name="description"]').attr('content', $metaData['meta_description']);
                $('head meta[name="keywords"]').attr('content', $metaData['meta_keyword']);
                document.title = $metaData['meta_title'];
            }
        },
        _ResetImage: function ($config) {
            var images = this.options.jsonChildProduct['image'],
                $this = this.element,
                context = $this.parents('.column.main'),
                gallery = context.find(this.options.mediaGallerySelector).data('gallery');
            gallery.updateData(images);
        },
        _ResetUrl: function ($config) {
            if ($config > 0) {
                window.history.replaceState(null, null, this.options.jsonChildProduct['url']);
            }
        },

        /**
         * Update total price
         *
         * @private
         */
        _UpdatePrice: function () {
            var $widget = this,
                index = '',
                $product = $widget.element.parents($widget.options.selectorProduct),
                $productPrice = $product.find(this.options.selectorProductPrice),
                options = _.object(_.keys($widget.optionsMap), {}),
                childData = $widget.options.jsonChildProduct['child'],
                result = {
                    oldPrice: {amount: 0},
                    basePrice: {amount: 0},
                    finalPrice: {amount: 0}
                };

            if ($widget.options.jsonChildProduct['is_ajax_load'] > 0) {
                return;
            }
            $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                index += $(this).attr('option-selected') + '_';
            });
            var $taxRate,
                $sameRateAsStore;

            if (childData.hasOwnProperty(index)) {
                result.oldPrice.amount = Number(childData[index]['price']['oldPrice']);
                result.basePrice.amount = Number(childData[index]['price']['basePrice']);
                result.finalPrice.amount = Number(childData[index]['price']['finalPrice']);
            } else {
                result.oldPrice.amount = Number($widget.options.jsonChildProduct['price']['finalPrice']);
                result.basePrice.amount = Number($widget.options.jsonChildProduct['price']['finalPrice']);
                result.finalPrice.amount = Number($widget.options.jsonChildProduct['price']['finalPrice']);
            }

            $productPrice.trigger(
                'updatePrice',
                {
                    'prices': $widget._getPrices(result, $productPrice.priceBox('option').prices)
                }
            );
            if (result.oldPrice.amount !== result.finalPrice.amount) {
                $(this.options.slyOldPriceSelector).show();
            } else {
                $(this.options.slyOldPriceSelector).hide();
            }
        },

        _UpdatePriceAjax: function ($prices) {
            var $widget = this,
                $product = $widget.element.parents($widget.options.selectorProduct),
                $productPrice = $product.find(this.options.selectorProductPrice),
                options = _.object(_.keys($widget.optionsMap), {}),
                result = {
                    oldPrice: {amount: 0},
                    basePrice: {amount: 0},
                    finalPrice: {amount: 0}
                };


            result.oldPrice.amount = Number($prices['oldPrice']);
            result.basePrice.amount = Number($prices['basePrice']);
            result.finalPrice.amount = Number($prices['finalPrice']);

            $productPrice.trigger(
                'updatePrice',
                {
                    'prices': $widget._getPrices(result, $productPrice.priceBox('option').prices)
                }
            );
            if (result.oldPrice.amount !== result.finalPrice.amount) {
                $(this.options.slyOldPriceSelector).show();
            } else {
                $(this.options.slyOldPriceSelector).hide();
            }
        },

        _getFormattedPrice: function (price) {
            return priceUtils.formatPrice(price, this.options.fomatPrice);
        }
    });

    return $.sz.Sdcp;
});
