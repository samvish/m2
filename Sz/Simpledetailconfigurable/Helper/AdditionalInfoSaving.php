<?php
namespace Sz\Simpledetailconfigurable\Helper;

class AdditionalInfoSaving
{
    private $preselectKey;

    private $productEnabledModule;

    public function __construct(
        \Sz\Simpledetailconfigurable\Model\ResourceModel\PreselectKey $preselectKey,
        \Sz\Simpledetailconfigurable\Model\ResourceModel\ProductEnabledModule $productEnabledModule
    ) {
        $this->preselectKey = $preselectKey;
        $this->productEnabledModule = $productEnabledModule;
    }

    public function savePreselectKey($postData, $productId)
    {
        $this->preselectKey->deleteOldKey($productId);
        foreach ($postData['sdcp_preselect'] as $key => $value) {
            $this->preselectKey->savePreselectKey($productId, $key, $value);
        }
    }

    public function saveEnabledModuleOnProduct($productId, $data)
    {
        $this->productEnabledModule->deleteOldKey($productId);
        $this->productEnabledModule->saveEnabled($productId, $data['enabled'], $data['is_ajax_load']);
    }
}
