<?php
namespace Sz\Simpledetailconfigurable\Helper;

use Magento\Customer\Api\GroupManagementInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class ProductData extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $productInfo;

    private $stockRegistry;

    private $configurableData;

    private $imageBuilder;

    private $imageHelper;

    private $productHelper;

    private $preselectKey;

    private $productEnabledModule;

    private $filterProvider;

    private $moduleConfig;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productInfo,
        \Magento\CatalogInventory\Model\StockRegistry $stockRegistry,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableData,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Helper\Product $productHelper,
        \Sz\Simpledetailconfigurable\Model\ProductEnabledModuleFactory $productEnabledModule,
        \Sz\Simpledetailconfigurable\Model\PreselectKeyFactory $preselectKey,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Sz\Simpledetailconfigurable\Helper\ModuleConfig $moduleConfig
    ) {
        $this->productInfo = $productInfo;
        $this->stockRegistry = $stockRegistry;
        $this->configurableData = $configurableData;
        $this->imageBuilder = $imageBuilder;
        $this->imageHelper = $imageHelper;
        $this->productHelper = $productHelper;
        $this->preselectKey = $preselectKey;
        $this->productEnabledModule = $productEnabledModule;
        $this->filterProvider = $filterProvider;
        $this->moduleConfig = $moduleConfig;
    }

    public function getAllData($productEntityId)
    {
        $result = [];
        $map_r = [];
        $result['preselect'] = $this->getSelectingDataWithConfig($productEntityId);
        
        $result['entity'] = $productEntityId;
        $isAjaxLoad = $this->getEnabledModuleOnProduct($productEntityId)['is_ajax_load'];
        $result['is_ajax_load'] = $isAjaxLoad;
        $product = $this->productInfo->getById($productEntityId);
        $this->getDetailData($product, $result);
        $this->getDetailStock($result, false);
        $result['url'] = $this->productHelper->getProductUrl($product);
        if ($result['url'] == null) {
            $result['url'] = str_replace(' ', '-', $result['name']) . $this->moduleConfig->getSuffix();
        }

        $parentAttribute = $this->configurableData->getConfigurableAttributes($product);
        foreach ($parentAttribute as $attrKey => $attrValue) {
            $attrCode = $attrValue->getProductAttribute()->getAttributeCode();
            $result['map2'][$attrCode]['code'] = $attrCode;
            $result['map2'][$attrCode]['id'] = $attrValue->getAttributeId();
            foreach ($product->getAttributes()[$attrValue->getProductAttribute()->getAttributeCode()]
                ->getOptions() as $tvalue) {
                $result['map'][$attrValue->getAttributeId()]['label'] = $attrValue->getLabel();
                $result['map'][$attrValue->getAttributeId()][$tvalue->getValue()] = $tvalue->getLabel();
                $map_r[$attrValue->getAttributeId()][$tvalue->getLabel()] = $tvalue->getValue();
            }
        }
        if ($isAjaxLoad) {
            $this->getDetailPrice($product, $result);
            $result['child'] = [];
            return $result;
        }

        
        $parentPrice = 0;

        $parentProduct = $this->configurableData->getChildrenIds($productEntityId);
        foreach ($parentProduct[0] as $simpleProduct) {
            $childProduct = [];
            $childProduct['entity'] = $simpleProduct;
            $child = $this->productInfo->getById($childProduct['entity']);
            $this->getDetailData($child, $childProduct);
            $this->getDetailStock($childProduct);
            $this->getDetailPrice($child, $childProduct);
            $key = '';
            foreach ($parentAttribute as $attrKey => $attrValue) {
                $attrCode = $attrValue->getProductAttribute()->getAttributeCode();
                $childRow = $child->getAttributes()[$attrCode]->getFrontend()->getValue($child);
                $attrKey = $child->getData($attrCode);
                $key .= $attrKey . '_';
                $result['map2'][$attrCode]['child'][$attrKey] = $childRow;
            }
            $result['child'][$key] = $childProduct;
            $parentPrice = $childProduct['price']['finalPrice'];
        }
        foreach ($result['child'] as $rk => $ri) {
            $parentPrice = ($ri['price']['finalPrice'] < $parentPrice) ? $ri['price']['finalPrice'] : $parentPrice;
        }
        $result['price']['finalPrice'] = $parentPrice;
        return $result;
    }

    public function getChildDetail($params)
    {
        $result = [];
        $product = $this->productInfo->getById($params['product_id']);
        $attributes = $this->configurableData->getConfigurableAttributes($product);
        if (count($attributes) !== count($params['super_attribute'])) {
            return false;
        }
        $child = $this->configurableData->getProductByAttributes($params['super_attribute'], $product);
        if ($child) {
            $child = $this->productInfo->getById($child->getId());
            $result['entity'] = $child->getId();
            $this->getDetailData($child, $result);
            $this->getDetailStock($result);
            $this->getDetailPrice($child, $result);
            return $result;
        }
        return false;
    }

    public function getDetailData($product, &$data)
    {
        $data['sku'] = $product->getSku();
        $data['name'] = $product->getName();
        $data['desc'] = $this->filterProvider->getPageFilter()->filter($product->getDescription());
        $data['sdesc'] = $this->filterProvider->getPageFilter()->filter($product->getShortDescription());
        $data['meta_data']['meta_title'] = $product->getMetaTitle();
        $data['meta_data']['meta_keyword'] = $product->getMetaKeyword();
        $data['meta_data']['meta_description'] = $product->getMetaDescription();
        $data['additional_info'] = $this->getAdditionalInfo($product);
        $data['image'] = $this->getGalleryImages($product);
    }

    public function getDetailStock(&$data, $isChild = true)
    {
        $childStock = $this->stockRegistry->getStockItem($data['entity']);
        $data['stock_number'] = $childStock->getQty();
        $data['stock_status'] = $childStock->getIsInStock();
        if ($isChild) {
            $data['minqty'] = ($childStock->getUseConfigMinSaleQty()) ? 0 : $childStock->getMinSaleQty();
            $data['maxqty'] = ($childStock->getUseConfigMaxSaleQty()) ? 0 : $childStock->getMaxSaleQty();
            $data['increment'] = ($childStock->getUseConfigQtyIncrements()) ? 0 : $childStock->getQtyIncrements();
        }
    }

    public function getDetailPrice($product, &$data)
    {
        $data['price']['oldPrice'] = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();
        $data['price']['basePrice'] = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount();
        $data['price']['finalPrice'] = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
        $data['price']['tier_price'] = $this->getTierPriceData($product);
    }
    
    public function getSelectingKey($productId)
    {
        $result = [];
        $parentProduct = $this->configurableData->getChildrenIds($productId);
        $product = $this->productInfo->getById($productId);
        $parentAttribute = $this->configurableData->getConfigurableAttributes($product);
        foreach ($parentProduct[0] as $simpleProduct) {
            $child = $this->productInfo->getById($simpleProduct);
            foreach ($parentAttribute as $attrKey => $attrValue) {
                $attrLabel = $attrValue->getProductAttribute()->getAttributeCode();
                if (!array_key_exists($attrLabel, $child->getAttributes())) {
                    continue;
                }
                $result[$attrValue->getAttributeId()]['label'] = $attrValue->getLabel();
                $childRow = $child->getAttributes()[$attrLabel]->getFrontend()->getValue($child);
                $result[$attrValue->getAttributeId()]['child'][$child->getData($attrLabel)] = $childRow;
            }
        }
        return $result;
    }

    public function getSelectingData($productId)
    {
        $result = [];
        $collection = $this->preselectKey->create()
        ->getCollection()
        ->addFieldToFilter('product_id', $productId);
        foreach ($collection as $value) {
            $result[$value['attribute_key']] = $value['value_key'];
        }
        return $result;
    }

    public function getSelectingDataWithConfig($product)
    {
        $result = [];
        $result['data'] = $this->getSelectingData($product);
        if ($result['data'] != null) {
            $result['enabled'] = true;
        } else {
            $result['enabled'] = false;
        }
        return $result;
    }

    public function getEnabledModuleOnProduct($productId)
    {
        $resultObject = $this->productEnabledModule->create()->load($productId);
        if (!$resultObject->getProductId()) {
            return $this->productEnabledModule->create()->setData(['enabled' => 1, 'is_ajax_load' => 0]);
        }
        return $resultObject;
    }

    public function getPrice($productPrices, $basePrice, $customerId)
    {
        $customerPrice = [];
        $result = [];
        foreach ($productPrices as $key => $price) {
            if (($price['id'] == '32000' || $price['id'] == $customerId)) {
                if (array_key_exists($price['qty'], $customerPrice)) {
                    $customerPrice[$price['qty']]['value'] = min(
                        $customerPrice[$price['qty']]['value'],
                        $price['value']
                    );
                } else {
                    $customerPrice[$price['qty']]['qty'] = $price['qty'];
                    $customerPrice[$price['qty']]['value'] = $price['value'];
                }
            }
        }
        return $customerPrice;
    }

    public function getTierPriceData($product)
    {
        $result = [];
        $finalPrice = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
        $baseFinalPrice = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount();
        $tierPricesList = $product->getPriceInfo()->getPrice('tier_price')->getTierPriceList();
        if (isset($tierPricesList) && !empty($tierPricesList)) {
            foreach ($tierPricesList as $key => $tier) {
                $tierData = [];
                $tierData['qty'] = $tier['price_qty'];
                $tierData['final'] = $tier['price']->getValue();
                $tierData['value'] = $tier['price']->getValue();
                $tierData['base'] = $tier['price']->getBaseAmount();
                $tierData['final_discount'] = $tierData['final'] - $finalPrice;
                $tierData['base_discount'] = $tierData['base'] - $baseFinalPrice;
                $tierData['percent'] = (1 - $tierData['base']/$baseFinalPrice) * 100;
                $result[$tierData['qty']] = $tierData;
            }
        }
        return $result;
    }

    public function getGalleryImages($product)
    {
        $images = $product->getMediaGalleryImages();
        $imagesItems = [];
        if ($images instanceof \Magento\Framework\Data\Collection) {
            foreach ($images as $image) {
                $image->setData(
                    'small_image_url',
                    $this->imageHelper->init($product, 'product_page_image_small')
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'medium_image_url',
                    $this->imageHelper->init($product, 'product_page_image_medium')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'large_image_url',
                    $this->imageHelper->init($product, 'product_page_image_large')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $imagesItems[] = [
                    'thumb' => $image->getData('small_image_url'),
                    'img' => $image->getData('medium_image_url'),
                    'full' => $image->getData('large_image_url'),
                    'caption' => $image->getLabel(),
                    'position' => $image->getPosition(),
                    'isMain' => $product->getImage() == $image->getFile(),
                ];
            }
        }
        if (empty($imagesItems)) {
            $imagesItems[] = [
                'thumb' => $this->imageHelper->getDefaultPlaceholderUrl('thumbnail'),
                'img' => $this->imageHelper->getDefaultPlaceholderUrl('image'),
                'full' => $this->imageHelper->getDefaultPlaceholderUrl('image'),
                'caption' => '',
                'position' => '0',
                'isMain' => true,
            ];
        }
        return $imagesItems;
    }

    public function getAdditionalInfo($product)
    {
        $result = [];
        foreach ($product->getAttributes() as $attrkey => $value) {
            if ($value->getData('is_visible_on_front')) {
                $valueData = $value->getFrontend()->getValue($product);
                if ($valueData != false && $valueData != 'No' && $valueData != 'N/A') {
                    $result[$attrkey]['value'] = $valueData;
                    $result[$attrkey]['label'] = $value->getData('frontend_label');
                }
            }
        }
        return $result;
    }

    public function getProductRepository()
    {
        return $this->productInfo;
    }
}
