**Simple Configurable Module**

*Description*
- Module can be used to switch the content of configurable product option selection. including URL
---

## How to Enable Simple Details on Configurable Product Extension?
1. Go To Admin -> System -> Configuration -> CONFIGURABLE PRODUCTS -> Simple Details on Configurable Product
2. Set Enable flag to Yes and configure the other setting based on requirement.for example, set SKU field from 
Attributes Display config, in order to see reflection of SKU change on configurable product option change
