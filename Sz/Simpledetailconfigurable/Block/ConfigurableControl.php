<?php

namespace Sz\Simpledetailconfigurable\Block;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product as CatalogProduct;
use Magento\ConfigurableProduct\Helper\Data;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Swatches\Helper\Data as SwatchData;
use Magento\Swatches\Helper\Media;

/**
 * Swatch renderer block
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ConfigurableControl extends \Magento\Swatches\Block\Product\Renderer\Configurable
{
    const SZ_SWATCH_RENDERER_TEMPLATE = 'Sz_Simpledetailconfigurable::SimpledetailControl.phtml';

    private $linkData;

    private $moduleConfig;

    private $customerSession;

    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        EncoderInterface $jsonEncoder,
        Data $helper,
        CatalogProduct $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        SwatchData $swatchHelper,
        Media $swatchMediaHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Sz\Simpledetailconfigurable\Helper\ProductData $linkData,
        \Sz\Simpledetailconfigurable\Helper\ModuleConfig $moduleConfig,
        array $data = []
    ) {
        $this->linkData = $linkData;
        $this->moduleConfig = $moduleConfig;
        $this->customerSession = $customerSession;
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $swatchHelper,
            $swatchMediaHelper,
            $data
        );
    }

    /**
     * sz_Simpledetailconfigurable
     * Get child product data
     */
    public function getJsonChildProductData()
    {
        return $this->jsonEncoder->encode($this->linkData->getAllData($this->getProduct()->getEntityId()));
    }

    /**
     * sz_Simpledetailconfigurable
     * Get module config
     */
    public function getJsonModuleConfig()
    {

        return $this->jsonEncoder->encode($this->moduleConfig->getAllConfig());
    }

    public function getAllowProducts()
    {
        if (!$this->moduleConfig->isModuleEnable() || !$this->moduleConfig->isShowStockStatus()) {
            return parent::getAllowProducts();
        }
        if (!$this->hasAllowProducts()) {
            $products = $this->getProduct()->getTypeInstance()->getUsedProducts($this->getProduct(), null);
            $this->setAllowProducts($products);
        }
        return $this->getData('allow_products');
    }

    /**
     * Get Key for caching block content
     *
     * @return string
     */
    public function getRendererTemplate()
    {
        return self::SZ_SWATCH_RENDERER_TEMPLATE;
    }

    public function getCacheLifetime()
    {
        return null;
    }
}
