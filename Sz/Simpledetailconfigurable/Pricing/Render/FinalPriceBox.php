<?php

namespace Sz\Simpledetailconfigurable\Pricing\Render;

use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Catalog\Pricing\Price\RegularPrice;

class FinalPriceBox
{
    public function aroundGetPriceType(
        \Magento\ConfigurableProduct\Pricing\Render\FinalPriceBox $subject,
        $proceed,
        $priceCode
    ) {
        if ($subject->getSaleableItem()->hasSdcpPriceInfo()) {
            $subject->setCacheLifetime(0);
            return $subject->getSaleableItem()->getSdcpPriceInfo()->getPrice($priceCode);
        }
        return $proceed($priceCode);
    }
    public function afterHasSpecialPrice(
        \Magento\ConfigurableProduct\Pricing\Render\FinalPriceBox $subject,
        $result
    ) {
        $item = $subject->getSaleableItem();
        if ($item->hasSdcpPriceInfo()) {
            $regularPrice = $item->getSdcpPriceInfo()->getPrice(RegularPrice::PRICE_CODE)->getValue();
            $finalPrice = $item->getSdcpPriceInfo()->getPrice(FinalPrice::PRICE_CODE)->getValue();
            if ($finalPrice < $regularPrice) {
                return true;
            }
            return false;
        }
        return $result;
    }
}
