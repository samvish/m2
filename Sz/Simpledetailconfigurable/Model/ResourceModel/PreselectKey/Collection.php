<?php
namespace Sz\Simpledetailconfigurable\Model\ResourceModel\PreselectKey;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            'Sz\Simpledetailconfigurable\Model\PreselectKey',
            'Sz\Simpledetailconfigurable\Model\ResourceModel\PreselectKey'
        );
    }
}
