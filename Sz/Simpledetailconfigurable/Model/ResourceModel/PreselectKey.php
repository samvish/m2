<?php
namespace Sz\Simpledetailconfigurable\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class PreselectKey extends AbstractDb
{
    public function _construct()
    {
        $this->_init('sdcp_preselect', 'preselect_id');
    }

    public function savePreselectKey($productId, $key, $value)
    {
        $connection = $this->getConnection();
        $bind = [
            'product_id' => $productId,
            'attribute_key' => $key,
            'value_key' => $value
        ];
        if ($value != '') {
            $connection->insert($this->getMainTable(), $bind);
        }
    }
    
    public function deleteOldKey($productId)
    {
        $this->getConnection()->delete($this->getMainTable(), ['product_id=?' => $productId]);
    }
}
