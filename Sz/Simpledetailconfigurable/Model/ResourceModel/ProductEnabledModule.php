<?php

namespace Sz\Simpledetailconfigurable\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ProductEnabledModule extends AbstractDb
{
    public function _construct()
    {
        $this->_init('sdcp_product_enabled', 'product_id');
    }

    public function saveEnabled($productId, $enabled, $isAjax)
    {
        $connection = $this->getConnection();
        $bind = [
            'product_id' => $productId,
            'enabled' => $enabled,
            'is_ajax_load' => $isAjax
        ];
        $connection->insert($this->getMainTable(), $bind);
    }
    
    public function deleteOldKey($productId)
    {
        $this->getConnection()->delete($this->getMainTable(), ['product_id=?' => $productId]);
    }
}
