<?php
namespace Sz\Simpledetailconfigurable\Model;

use Magento\Framework\Model\AbstractModel;

class PreselectKey extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Sz\Simpledetailconfigurable\Model\ResourceModel\PreselectKey');
    }
}
