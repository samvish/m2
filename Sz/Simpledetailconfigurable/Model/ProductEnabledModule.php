<?php

namespace Sz\Simpledetailconfigurable\Model;

use Magento\Framework\Model\AbstractModel;

class ProductEnabledModule extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Sz\Simpledetailconfigurable\Model\ResourceModel\ProductEnabledModule');
    }
}
