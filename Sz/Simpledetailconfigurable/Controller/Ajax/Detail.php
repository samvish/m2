<?php

namespace Sz\Simpledetailconfigurable\Controller\Ajax;
 
use Magento\Framework\App\Action\Context;
 
class Detail extends \Magento\Framework\App\Action\Action
{
    private $productData;

    private $resultJsonFactory;

    public function __construct(
        Context $context,
        \Sz\Simpledetailconfigurable\Helper\ProductData $productData,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->productData = $productData;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory  = $resultPageFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {   $this->resultPageFactory->create();
        $resultJson = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $param = $this->getRequest()->getParams();
            $result = $this->productData->getChildDetail($param);
            return $resultJson->setData($result);
        } else {
            return $resultJson->setData(null);
        }
    }
}
