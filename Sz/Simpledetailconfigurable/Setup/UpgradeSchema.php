<?php

namespace Sz\Simpledetailconfigurable\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addProductEnabledTable($setup);
        }

        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $this->addAjaxDetailColumn($setup);
        }
        $setup->endSetup();
    }
    public function addProductEnabledTable($setup)
    {
        if (!$setup->tableExists('sdcp_product_enabled')) {
            $table = $setup->getConnection()
                ->newTable(
                    $setup->getTable('sdcp_product_enabled')
                )
                ->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['primary' => true, 'nullable' => false],
                    'Product Id'
                )
                ->addColumn(
                    'enabled',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false],
                    'Enabled'
                )->addIndex(
                    $setup->getIdxName('sdcp_product_enabled', ['product_id']),
                    ['product_id']
                )
                ->setComment(
                    'Preselect key for configurable product'
                );
                $setup->getConnection()->createTable($table);
        }
    }

    public function addAjaxDetailColumn($setup)
    {
        $szSdcpTable = $setup->getTable('sdcp_product_enabled');
        $connection = $setup->getConnection();
        if ($connection->isTableExists($szSdcpTable)) {
            $connection->addColumn(
                $szSdcpTable,
                'is_ajax_load',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'length' => 6,
                    'nullable' => false,
                    'comment' => 'enable ajax load'
                ]
            );
        }
    }
}
