<?php

namespace Sz\Simpledetailconfigurable\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductInitObserver implements ObserverInterface
{
    private $urlIdentifier;

    private $moduleConfig;

    public function __construct(
        \Sz\Simpledetailconfigurable\Helper\UrlIdentifier $urlIdentifier,
        \Sz\Simpledetailconfigurable\Helper\ModuleConfig $moduleConfig
    ) {
        $this->urlIdentifier = $urlIdentifier;
        $this->moduleConfig = $moduleConfig;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getControllerAction()->getRequest();
        $product = $observer->getProduct();
        if ($request->getFullActionName() === 'catalog_product_view' && $product->getTypeId() === 'configurable') {
            $product = $observer->getProduct();
            $pathInfo = $request->getOriginalPathInfo();
            $product->setSdcpData($pathInfo);
            $moduleConfig = $this->moduleConfig->getAllConfig();
            try {
                $child = $this->urlIdentifier->getChildProduct($this->removeFirstSlashes($pathInfo));
            } catch (\Exception $e) {
                $child = null;
            }
            if ($child) {
                $product->setSdcpPriceInfo($child->getPriceInfo());
                $product->setSdcpId($pathInfo);
                if ($moduleConfig['sku']) {
                    $product->setSku($child->getSku());
                }
                if ($moduleConfig['name']) {
                    $product->setName($child->getName());
                }
                if ($moduleConfig['meta_data']) {
                    if ($child->hasMetaTitle()) {
                        $product->setMetaTitle($child->getMetaTitle());
                    }
                    if ($child->hasMetaKeyword()) {
                        $product->setMetaKeyword($child->getMetaKeyword());
                    }
                    if ($child->hasMetaDescription()) {
                        $product->setMetaDescription($child->getMetaDescription());
                    }
                }
            }
        }
    }

    protected function removeFirstSlashes($pathInfo)
    {
        $firstChar = (string)substr($pathInfo, 0, 1);
        if ($firstChar == '/') {
            $pathInfo = ltrim($pathInfo, '/');
        }

        return $pathInfo;
    }
}
