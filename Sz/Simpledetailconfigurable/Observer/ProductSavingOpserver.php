<?php

namespace Sz\Simpledetailconfigurable\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer as EventObserver;

class ProductSavingOpserver implements ObserverInterface
{
    private $additionalInfoSaving;

    private $moduleConfig;

    public function __construct(
        \Sz\Simpledetailconfigurable\Helper\AdditionalInfoSaving $additionalInfoSaving,
        \Sz\Simpledetailconfigurable\Helper\ModuleConfig $moduleConfig
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->additionalInfoSaving = $additionalInfoSaving;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $postData = $observer->getData('controller')->getRequest()->getPost('product');
        $productId = ($observer->getData('product')) ? $observer->getData('product')->getEntityId()
        : $postData['54_preselect_id'];

        if ($this->moduleConfig->isModuleEnable() && array_key_exists('sdcp_preselect', $postData)) {
            $this->additionalInfoSaving->savePreselectKey($postData, $productId);
        }

        if ($this->moduleConfig->isModuleEnable() && array_key_exists('sdcp_general', $postData)) {
            $this->additionalInfoSaving->saveEnabledModuleOnProduct($productId, $postData['sdcp_general']);
        }
    }
}
