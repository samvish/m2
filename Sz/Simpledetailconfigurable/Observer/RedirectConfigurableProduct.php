<?php

namespace Sz\Simpledetailconfigurable\Observer;

use Magento\Framework\Event\ObserverInterface;

class RedirectConfigurableProduct implements ObserverInterface
{
    private $urlIdentifier;

    private $moduleConfig;
    
    public function __construct(
        \Sz\Simpledetailconfigurable\Helper\UrlIdentifier $urlIdentifier,
        \Sz\Simpledetailconfigurable\Helper\ModuleConfig $moduleConfig
    ) {
        $this->urlIdentifier = $urlIdentifier;
        $this->moduleConfig = $moduleConfig;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getData('request');
        if ($this->moduleConfig->isModuleEnable() && $this->moduleConfig->customUrl()) {
            $redirectUrl = $this->urlIdentifier->readUrl($request->getOriginalPathInfo());
            if ($redirectUrl['product'] != '0') {
                $controllerRequest = $observer->getData('controller_action')->getRequest();
                $controllerRequest->initForward();
                $params = ['id' => $redirectUrl['product'], 'category' => $redirectUrl['category']];
                $controllerRequest->setParams($params);
                $controllerRequest->setModuleName('catalog');
                $controllerRequest->setControllerName('product');
                $controllerRequest->setActionName('view');
                $controllerRequest->setDispatched(false);
            }
        }
    }
}
